import React, {memo, useState, createRef, useCallback, useEffect} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import Modal from 'modal-react-native-web';
import {ScreenShot} from '../../../core/api/screenShot';
import {share} from '../../../core/api/share';
import {Fingerprint} from '../../../core/api/fingerprint';
export const MainWebScreen = memo(() => {
  const [isModal, setModal] = useState(false);
  const [type, setType] = useState('');
  const captureRef = createRef();

  const handleShareModal = useCallback((type) => {
    setType(type);
    setModal(true);
  });

  const handleShare = useCallback(
    (app) => {
      if (type == 'text') {
        share.text(app, [{label: 'hello', value: 'world'}]);
      } else if (type == 'link') {
        share.link(app, 'google.com');
      } else if(type == 'screen'){
        ScreenShot.shot(captureRef.current)
      }
    },
    [type],
  );
  const handleFingerPrintResponse = useCallback(async() => {
     try{
      let response = await Fingerprint.handleFingerprint();
      console.log(response)
     }catch(e){
       console.log(e)
     }
  },[])
  useEffect(() => {
   handleFingerPrintResponse ()
  },[])
  return (
    <>
      <Modal visible={isModal}>
        <TouchableOpacity onPress={() => setModal(false)} style={styles.close}>
          <Text style={styles.text}> Close </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('whatsApp')}
          style={styles.btn}>
          <Text style={styles.text}> WhatsApp </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('mail')}
          style={styles.btn}>
          <Text style={styles.text}> Email </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('telegram')}
          style={styles.btn}>
          <Text style={styles.text}> Telegram </Text>
        </TouchableOpacity>
      </Modal>
      <View ref={captureRef} style={styles.container}>
        <TouchableOpacity
          onPress={() => handleShareModal('screen')}
          style={styles.btn}>
          <Text style={styles.text}> Share Screen </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShareModal('text')}
          style={styles.btn}>
          <Text style={styles.text}> Share Text </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShareModal('link')}
          style={styles.btn}>
          <Text style={styles.text}> Share Link </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShareModal('screen')}
          style={styles.btn}>
          <Text style={styles.text}> Share image test preview </Text>
        </TouchableOpacity>
      </View>
    </>
  );
});
