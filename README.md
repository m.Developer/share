## TOC

- [install](#install)
- [run](#run)
- [build](#build)
- [mobile](#mobile)
- [web](#web)
- [usage](#usage)

## install

- get clone of project

## run

- run
  - `$ yarn run [android|ios|web]`
- [iOS] Via Xcode
  - `open ios/funzi.xcodeproj` (open the project on Xcode)
  - Press the Run button
- [Android] Via Android Studio
  - `studio android/` (open the project on Android Studio)
  - Press the Run button

## build

- android
  - `$ ./gradlew assembleRelease`
  - `$ npx react-native run-android --variant=release`


## mobile

```javascript
import React, {memo, createRef} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {ScreenShot} from '../../../core/api/screenShot';
import {share} from '../../../core/api/share/index';

export const MainMobileScreen = memo(() => {
  const screenRef = createRef();
  return (
    <View ref={screenRef} style={styles.container}>
      <TouchableOpacity
        onPress={() => ScreenShot.shot(screenRef.current)}
        style={styles.btn}>
        <Text style={styles.text}> Share Screen </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => share.text([{label: 'hello', value: 'world'}])}
        style={styles.btn}>
        <Text style={styles.text}> Share Text </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => share.link('http://google.com')}
        style={styles.btn}>
        <Text style={styles.text}> Share Link </Text>
      </TouchableOpacity>
    </View>
  );
});
```


## web

```javascript
import React, {memo, useState, createRef, useCallback} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import Modal from 'modal-react-native-web';
import {ScreenShot} from '../../../core/api/screenShot/index.web';
import {share} from '../../../core/api/share/index.web';
export const MainWebScreen = memo(() => {
  const [isModal, setModal] = useState(false);
  const [type, setType] = useState('');
  const captureRef = createRef();

  const handleShareModal = useCallback((type) => {
    setType(type);
    setModal(true);
  });

  const handleShare = useCallback(
    (app) => {
      if (type == 'text') {
        share.text(app, [{label: 'hello', value: 'world'}]);
      } else if (type == 'link') {
        share.link(app, 'google.com');
      }
    },
    [type],
  );
  return (
    <>
      <Modal visible={isModal}>
        <TouchableOpacity onPress={() => setModal(false)} style={styles.close}>
          <Text style={styles.text}> Close </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('whatsApp')}
          style={styles.btn}>
          <Text style={styles.text}> WhatsApp </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('mail')}
          style={styles.btn}>
          <Text style={styles.text}> Email </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShare('telegram')}
          style={styles.btn}>
          <Text style={styles.text}> Telegram </Text>
        </TouchableOpacity>
      </Modal>
      <View ref={captureRef} style={styles.container}>
        <TouchableOpacity
          onPress={() => handleShareModal('screen')}
          style={styles.btn}>
          <Text style={styles.text}> Share Screen </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShareModal('text')}
          style={styles.btn}>
          <Text style={styles.text}> Share Text </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => handleShareModal('link')}
          style={styles.btn}>
          <Text style={styles.text}> Share Link </Text>
        </TouchableOpacity>
      </View>
    </>
  );
});

```

## usage

```javascript

  //send Text in mobile

    share.text([{label: 'subject', value: 'world'}]);

  //send link in mobile 

    share.link('http://google.com');

  //send ScreenShot in mobile

    ScreenShot.shot(your view ref);

  // send Text in web 

    share.text(app, [{label: 'hello', value: 'world'}]); // app : whatsapp , mail , telegram
  
  //send link in web

    share.link(app, 'google.com'); // app : whatsapp, mail , telegram

```
## Author

Email: [m.heydari.developer@gmail.com](email:m.heydari.developer@gmail.com)<br/>
