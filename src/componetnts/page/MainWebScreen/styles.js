import {StyleSheet} from 'react-native';
import {colors} from '../../../core/common';
import {dimensions} from '../../../core/dimensions';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    height: dimensions.height,
  },
  btn: {
    padding: 20,
    borderRadius: 15,
    backgroundColor: colors.surface,
    marginVertical: 15,
    marginHorizontal: 15,
  },
  close: {
    padding: 20,
    borderRadius: 8,
    backgroundColor: colors.error,
    marginVertical: 15,
    marginHorizontal: 15,
  },
  text: {
    color: colors.background,
  },


});
