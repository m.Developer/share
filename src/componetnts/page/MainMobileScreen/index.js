import React, {memo, createRef, useEffect, useCallback} from 'react';

import {View, Text, TouchableOpacity} from 'react-native';
import styles from './styles';
import {ScreenShot} from '../../../core/api/screenShot';
import {share} from '../../../core/api/share';
import {Fingerprint} from '../../../core/api/fingerprint';

export const MainMobileScreen = memo(() => {
  const screenRef = createRef();

  const handleFingerPrintResponse = useCallback(async () => {
    try {
      let response = await Fingerprint.handleFingerPrint(
        'Login With Biometrics',
      );
      // console.log(response)
      alert(`authonticate ${response.username}`);
    } catch (e) {
      console.log(e);
    }
  }, []);
  useEffect(() => {
    handleFingerPrintResponse();
  }, []);
  return (
    <View ref={screenRef} style={styles.container}>
      <TouchableOpacity
        onPress={() => ScreenShot.shot(screenRef.current)}
        style={styles.btn}>
        <Text style={styles.text}> Share Screen </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => share.text([{label: 'hello', value: 'world'}])}
        style={styles.btn}>
        <Text style={styles.text}> Share Text </Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => share.link('http://google.com')}
        style={styles.btn}>
        <Text style={styles.text}> Share Link </Text>
      </TouchableOpacity>
    </View>
  );
});
