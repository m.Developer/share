import {platform} from '../../platform';
import {Linking} from 'react-native';

function isPwa() {
  const toMatch = [
    /Android/i,
    /webOS/i,
    /iPhone/i,
    /iPad/i,
    /iPod/i,
    /BlackBerry/i,
    /Windows Phone/i,
  ];

  return toMatch.some((toMatchItem) => {
    return navigator.userAgent.match(toMatchItem);
  });
}
export const share = {
  async image(app, uri) {
    if (isPwa()) {
      const shareData = {
        title: 'poolkhord',
        uri: uri,
      };
      try {
        if (navigator.share) {
          return await navigator.share(shareData);
        } else {
          alert('please use ssl for using navigator');
        }
      } catch (e) {
        console.log(e);

        return false;
      }
    } else if (platform.isWeb) {
      let url = await share.createLinkUrl(app, uri);
      Linking.canOpenURL(url).then((supported) => {
        if (supported) {
          Linking.openURL(url);
        } else {
          alert('not opened');
        }
      });
    }
  },

  async text(app = null, texts) {
    if (isPwa()) {
      let text = '';
      texts.forEach((item) => {
        text = `${item.label} : \t \t ${item.value}\n`;
      });
      const shareData = {
        title: 'poolkhord',
        text,
      };
      try {
        if (navigator.share) {
          return await navigator.share(shareData);
        } else {
          alert('please use ssl for using navigator');
        }
      } catch (e) {
        console.log(e);

        return false;
      }
    } else if (platform.isWeb) {
      let text;
      texts.forEach((item) => {
        text = `${item.label + ' ' + item.value}`;
      });
      let url = await share.createLinkUrl(app, text);
      Linking.canOpenURL(url).then((supported) => {
        if (supported) {
          Linking.openURL(url);
        } else {
          alert('not opened');
        }
      });
    }
  },

  async link(app = null, link) {
    let url = await share.createLinkUrl(app, link);
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        alert('not opend');
      }
    });
  },
  async createLinkUrl(app, text) {
    let url = '';
    switch (app) {
      case 'whatsApp':
        url = `https://wa.me/?text=${text}`;
        break;
      case 'mail':
        url = `mailto:?body=${text}`;
        break;
      case 'telegram':
        url = `https://t.me/share/url?url=https://core.telegram.org/widgets/share&text=${text}`;
        break;
    }
    return url;
  },
};
