import {captureRef} from 'react-native-view-shot';
import {share} from '../share';

export const ScreenShot = {
  async shot(viewRef) {
    try {
      const uri = await ScreenShot.captureRef(viewRef);

      if (!uri) return;

      return share.image(uri);
    } catch (e) {
      return false;
    }
  },

  async captureRef(viewRef) {
    try {
      const uri = await captureRef(viewRef, {
        format: 'jpg',
        quality: 0.8,
      });
      return uri;
    } catch (e) {
      return false;
    }
  },
};
