import Share from 'react-native-share';

export const share = {
  async image(uri) {
    try {
      const options = {
        title: 'poolkhord',
        url: uri,
      };
      return Share.open(options);
    } catch (e) {
      console.log(e);

      return false;
    }
  },

  async text(texts) {
    try {
      let message = '';
      texts.forEach((item) => {
        message = `${item.label} : \t \t ${item.value}\n`;
      });
      const options = {
        title: 'poolkhord',
        message,
      };
      return Share.open(options);
    } catch (e) {
      console.log(e);

      return false;
    }
  },
  async link(text) {
    try {
      const options = {
        title: 'poolkhord',
        message: text,
      };
      return Share.open(options);
    } catch (e) {
      console.log(e);

      return false;
    }
  },
};
