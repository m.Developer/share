//colors component
const code = {
  ORANGE: '#FF7141',
  ORANGE_LIGHT: '#f7d8ce',
  WHITE: '#ffffff',
  GRAY: '#d0d7e4',
  GRAY_HOLDER: '#707070',
  GRAY_PALE: '#c4c4c4',
  GRAY_DARK: '#727c8e',
  GRAY_LIGHT: '#f5f5f5',
  BLACK_BOLD: '#515c6f',
  BLACK_LIGHT: '#434343',
  RED: '#f6e7e2',
};

export const colors = {
  background: code.WHITE,
  onBackground: code.GRAY,
  surface: code.ORANGE,
  onSurface: code.ORANGE_LIGHT,
  secondry: code.BLACK_BOLD,
  onSecondry: code.BLACK_LIGHT,
  thirdy: code.GRAY_LIGHT,
  sideBar: code.GRAY_DARK,
  error: code.RED,
  comments: code.GRAY_PALE,
  placeHolder: code.GRAY_HOLDER,
};

/**
 * @type {{
 * background: string,
 * onBackground: string,
 * surface: string,
 * onSurface: string,
 * secondry: string,
 * onSecondry: string,
 * thirdy: string,
 * sideBar: string,
 * error: string,
 * comments: string,
 * placeHolder: string,
 * }}
 */
