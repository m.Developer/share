import React from 'react';
import {StatusBar} from 'react-native';
import {platform} from './core';
import {MainMobileScreen, MainWebScreen} from './componetnts/page';

const App = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      {platform.isAndroid ? <MainMobileScreen /> : <MainWebScreen />}
    </>
  );
};

export default App;
