import {StyleSheet} from 'react-native';
import {colors} from '../../../core/common';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.background,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btn: {
    padding: 20,
    borderRadius: 15,
    backgroundColor: colors.surface,
    marginVertical: 15,
  },
  text: {
    color: colors.background,
  },
});
