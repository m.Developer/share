import html2canvas from 'html2canvas';
import {share} from '../share';
export const ScreenShot = {
  async shot(viewRef) {
    let canvas = await html2canvas(viewRef);
    return share.image(canvas.toDataURL());
  },
};
