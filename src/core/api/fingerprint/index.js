import FingerprintScanner from 'react-native-fingerprint-scanner';
import {platform} from '../../platform';
import * as Keychain from 'react-native-keychain';
export const Fingerprint = {
  async requiresLegacyAuthentication() {
    return (await platform.version) < 23;
  },
  async handleAuthenticationAttemptedLegacy(error) {
    return await error.message;
  },

  async handleFingerPrint(title) {
    let legecy = await Fingerprint.requiresLegacyAuthentication();
    if (legecy) {
      return await Fingerprint.authLegacy();
    } else {
      return await Fingerprint.authCurrent(title);
    }
  },

  async authCurrent(title) {
    try {
      let fingerResponse = await FingerprintScanner.authenticate({title});
      if (fingerResponse) {
        const username = 'test';
        const password = 'tester';
        await Keychain.setGenericPassword(username, password);
        return await Keychain.getGenericPassword();
      } else {
        return fingerResponse;
      }
    } catch (e) {
      return e;
    }
  },

  async authLegacy() {
    try {
      let response = await FingerprintScanner.authenticate({
        onAttempt: await Fingerprint.handleAuthenticationAttemptedLegacy(),
      });
      return response;
    } catch (e) {
      return e;
    }
  },

  async handleUnMount() {
    return await FingerprintScanner.release();
  },
};
